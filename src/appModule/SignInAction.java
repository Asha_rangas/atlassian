package appModule;
/*
 * This Class contains methods to set various fields required to
 * Sign into JIRA application and methods to login to the application
 */
 
import org.openqa.selenium.WebDriver;

import pageObjects.LogInPage;
import pageObjects.HomePage;
import utility.ExcelUtils;
import utility.Log;
       
public class SignInAction{
 
   public static void Execute(WebDriver driver)throws Exception{
 
	   String sUsername = ExcelUtils.getCellData(1, 1);
	   
	   Log.info("Username picked from Excel is "+ sUsername );
	   
	   String sPassword = ExcelUtils.getCellData(1, 2);
	   
	   Log.info("Password picked from Excel is "+ sPassword );
	   
	   HomePage.lnk_MyAccount(driver).click();
	 
	   LogInPage.txtbx_UserName(driver).sendKeys(sUsername);
	   
	   Log.info("Username entered in the Username text box");
	 
	   LogInPage.txtbx_Password(driver).sendKeys(sPassword);
	 
	   Log.info("Password entered in the Password text box");
	   
	   LogInPage.btn_LogIn(driver).click();
	   
	   Log.info("Click action performed on Submit button");
 
   }
 
}
package appModule;
/*
 * This Class contains methods to set various fields required to
 * Edit an Issue in JIRA application and methods to Edit an issue.
 */
import java.io.IOException;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import pageObjects.EditIssue;
import pageObjects.HomePage;
import pageObjects.SearchPage;
import utility.Log;
import utility.Constants;

public class EditIssueAction {
	/*
	 * sets the Summary Field in the Edit Issue Page
	 */
	private static void setSummary(WebDriver driver,String summary){
        WebElement summaryfield = EditIssue.txtbx_SummaryField(driver);
        summaryfield.sendKeys(summary);		
	}
	/*
	 * sets the Description Field in the Edit Issue Page
	 */
	private static void setDescription(WebDriver driver,String description){
        WebElement descriptionfield = EditIssue.txtbx_DescriptionField(driver);
        descriptionfield.sendKeys(description);		
	}
	/*
	 * Populates the search field with the required Defect_ID
	 */
	private static void searchDefect(WebDriver driver,String defect_id){
        WebElement searchfield = HomePage.txtbx_QuickSearch(driver);
        searchfield.click();
        searchfield.sendKeys(defect_id);	
        searchfield.sendKeys(Keys.ENTER);
	}
	/*
	 * Clicks the Edit option of the defect ID
	 */
	private static void editDefect(WebDriver driver){
        WebElement edit = HomePage.btn_EditIssue(driver);
        edit.click();
        
	}
	/*
	 * Clicks on the Issues Submitted by me link 
	 */
	private static void getIssuesbyMe(WebDriver driver){
        WebElement search = HomePage.lnk_SearchIssue(driver);
        Actions action = new Actions(driver);
        action.click(search).build().perform();
        HomePage.lnk_SearchIssueReportedbyme(driver).click();       
	}
	/*
	 * This methods performs all the actions required to Edit an issue
	 * or search an issue based on the value of the FLAG .
	 * If flag == 1 -- Edit option is performed
	 * if flag == 2 -- Search option is performed.
	 */
	public static void Execute(WebDriver driver,int flag)throws Exception{
		
		getIssuesbyMe(driver);
		String defect_id= SearchPage.get_DefectId(driver, flag);
		
		if(defect_id == Constants.ERROR){
			throw new IOException("There are no defects submitted by you.Please create one !!");
		}
		
		if(flag == 1){
						  
			String sSummary="Editing summary of defect id "+defect_id ;
			String sDescription="Editing description of defect id "+defect_id ;
					
			searchDefect(driver,defect_id);
			editDefect(driver);		
			setSummary(driver,sSummary);
			setDescription(driver,sDescription);		   
			EditIssue.btn_EditIssueSubmit(driver).click();
			driver.get(driver.getCurrentUrl());  
			Log.info("Click action performed on Edit button");
			
		}else if (flag == 2){
			
			searchDefect(driver,defect_id);		
			String url = driver.getCurrentUrl();
			Log.info("Currenturl = "+url);
		}
	 
	   }

}

package appModule;
/*
 * This Class contains methods to set various fields required to
 * Create an Issue in JIRA application and methods to submit an issue.
 */
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.*;
import utility.ExcelUtils;
import utility.Log;

public class CreateIssueAction {
	/*
	 * sets the Project Field in the Create Issue Page
	 */
	private static void setProject(WebDriver driver,String project_name){
        WebElement project = CreateIssue.txtbx_ProjectField(driver);   
        project.clear();
        project.sendKeys(project_name);
        project.sendKeys(Keys.ARROW_DOWN);
        project.sendKeys(Keys.ENTER);        
        System.out.println("Project Selected");		
	}
	/*
	 * sets the Issue_type Field in the Create Issue Page
	 */	
	private static void setIssue(WebDriver driver,String issue_type){
        WebElement issue = CreateIssue.txtbx_IssueTypeField(driver);
        issue.clear();
        issue.sendKeys(issue_type);
        issue.sendKeys(Keys.ARROW_DOWN);
        issue.sendKeys(Keys.ENTER);        
        System.out.println("Issue Selected");		
	}
	/*
	 * sets the Summary Field in the Create Issue Page
	 */	
	private static void setSummary(WebDriver driver,String summary){
        WebElement summaryfield = CreateIssue.txtbx_SummaryField(driver);
        summaryfield.sendKeys(summary);		
	}
	/*
	 * sets the Description Field in the Create Issue Page
	 */	
	private static void setDescription(WebDriver driver,String description){
        WebElement descriptionfield = CreateIssue.txtbx_DescriptionField(driver);
        descriptionfield.sendKeys(description);		
	}
	/*
	 * This method Clicks on the Create button and sets the mandatory 
	 * Parameters required to Create an issue and Submits the form.
	 */
	public static void Execute(WebDriver driver)throws Exception{
		 
		 String sProject ,sIssueType,sSummary,sDescription ;
			
		 sProject = ExcelUtils.getCellData(1, 4);  
		 sIssueType = ExcelUtils.getCellData(1, 5);
		 sSummary = ExcelUtils.getCellData(1, 6);
		 sDescription = ExcelUtils.getCellData(1, 7);		
		 
		 HomePage.lnk_CreateIssue(driver).click();
	
		 setProject(driver,sProject);		   
		 setIssue(driver,sIssueType);
		 setSummary(driver,sSummary);
		 setDescription(driver,sDescription);
		 
		 CreateIssue.btn_CreateIssueSubmit(driver).click();
		 	   
		 Log.info("Click action performed on Submit button");
	 
	   }

}

package testRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BasicFlowofAllTestCases {
	
	
	/**
     * Attempts to click on an element multiple times (to avoid stale element
     * exceptions caused by rapid DOM refreshes)
     *
     * @param d
     *            The WebDriver
     * @param by
     *            By element locator
     */
    public static void dependableClick(WebDriver d, By by)
    {
        final int MAXIMUM_WAIT_TIME = 20;
        final int MAX_STALE_ELEMENT_RETRIES = 5;

        WebDriverWait wait = new WebDriverWait(d, MAXIMUM_WAIT_TIME);
        int retries = 0;
        while (true)
        {
            try
            {
                wait.until(ExpectedConditions.elementToBeClickable(by)).click();

                return;
            }
            catch (StaleElementReferenceException e)
            {
                if (retries < MAX_STALE_ELEMENT_RETRIES)
                {
                    retries++;
                    continue;
                }
                else
                {
                    throw e;
                }
            }
        }
    }

	public static void main(String[] args) {
        // declaration and instantiation of objects/variables
        WebDriver driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
          
        String baseUrl = "https://jira.atlassian.com/browse/TST";
       
        // launch Firefox and direct it to the Base URL
        driver.get(baseUrl);
        driver.findElement(By.xpath(".//*[@id='user-options']/a")).click();
        driver.findElement(By.xpath(".//*[@id='username']")).clear();
        driver.findElement(By.xpath(".//*[@id='username']")).sendKeys("asha.r81@gmail.com");
        driver.findElement(By.xpath(".//*[@id='password']")).sendKeys("asha1234");
        driver.findElement(By.xpath(".//*[@id='login-submit']")).click();
        
        //File an Issue
               
        driver.findElement(By.xpath(".//*[@id='create_link']")).click(); 
        System.out.println("Clicked on create_link");        
 
        WebElement project = driver.findElement(By.xpath(".//*[@id='project-field']"));   
        project.clear();
        project.sendKeys("A Test Project");
        project.sendKeys(Keys.ARROW_DOWN);
        project.sendKeys(Keys.ENTER);        
        System.out.println("Project Selected");
        
        dependableClick(driver,By.id("issuetype-field"));        
        WebElement issue = driver.findElement(By.id("issuetype-field"));
        issue.clear();
        issue.sendKeys("Bug");
        issue.sendKeys(Keys.ARROW_DOWN);
        issue.sendKeys(Keys.ENTER);        
        System.out.println("Issue Selected");
        
        dependableClick(driver,By.id("summary"));
        driver.findElement(By.id("summary")).sendKeys("Summary of the new issue");
        dependableClick(driver,By.id("description"));
        driver.findElement((By.id("description"))).sendKeys("Description of the new issue");

        driver.findElement(By.id("create-issue-submit")).click();

        //Search Tab of JIRA
           dependableClick(driver,By.xpath(".//*[@id='find_link']")); 
	       WebElement search = driver.findElement(By.xpath(".//*[@id='find_link']"));
	       Actions action3 = new Actions(driver); 
	       action3.click(search).build().perform(); 
	       driver.findElement(By.xpath(".//*[@id='filter_lnk_reported_lnk']")).click(); 
	        
	       //List of issues that gets displayed
	       driver.findElements(By.xpath("//div[@class='list-content']")); 
	       List<WebElement> allElements = driver.findElements(By.xpath("//ol[@class='issue-list']/li")); 
	     
	       System.out.println("length of the allelements="+allElements.size());
	       List<String> myList = new ArrayList<>();
	       for (int i = 1; i < allElements.size()+1; i++) {
	           WebElement linkElement = driver
	                   .findElement(By
	                           .xpath("//div[@class='list-content']/ol/li[" + i
	                                   + "]/a/span[1]"));
	
	           System.out.println(linkElement.getText());
	           myList.add(linkElement.getText());
	       }
	       
	       System.out.println("length of the myList="+myList.size());
	       String searchDefect = "" ;
	       String editDefect = "";
	       for(int i = 0; i < myList.size(); i++) {
	    	   //Printing the defect ids
	           System.out.println("mylist="+myList.get(i));
	           if(i == 0){
	        	   searchDefect = myList.get(i);
	           }
	           if(i == 1){
	        	   editDefect = myList.get(i);
	           }
	        	   
	       }
	       
	       //Search for a given defect
	       WebElement searchfield = driver.findElement(By.xpath(".//*[@id='quickSearchInput']"));       
	       searchfield.click();
	       searchfield.sendKeys(searchDefect);
	       searchfield.sendKeys(Keys.ENTER);
	       
	       System.out.println("Search defect done");
	       
	       //Edit a given defect
	       WebElement editfield = driver.findElement(By.xpath(".//*[@id='quickSearchInput']"));       
	       editfield.click();
	       editfield.sendKeys(editDefect);
	       editfield.sendKeys(Keys.ENTER);
	       
	       WebElement edit = driver.findElement(By.xpath(".//*[@id='edit-issue']"));  
	       edit.click();
	       System.out.println("Clicked on edit button");
	       
	       WebElement summary = driver.findElement(By.xpath(".//*[@id='summary']"));
	       summary.clear();
	       summary.sendKeys("Editing summary of id "+editDefect);
	       
	       WebElement description = driver.findElement(By.xpath(".//*[@id='description']"));
	       description.clear();
	       description.sendKeys("Editing description of defect id "+editDefect);
	       driver.findElement(By.xpath(".//*[@id='edit-issue-submit']")).click();
	         
	        //close Firefox
	       driver.close();
	        
	        // exit the program explicitly
	       System.exit(0);
	    }
	    
    
    
}
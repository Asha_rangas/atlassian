package testRepository;
/*
 * This Class serves as a driver Class to execute all the testcases
 * of the suite.
 */
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test; 
import org.apache.log4j.xml.DOMConfigurator;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;

import utility.*;
import appModule.*;
 
   public class TestSuite {
 
	   	 public WebDriver driver = null;
	   	 
	   	@Test (priority = 0,enabled = true)
	   	 public void executeFirst_TC() throws Exception{
	   		 
	   		Log.startTestCase("JIRA_Test_001 - Create an Issue in JIRA"); 
	   		try{
	   			CreateIssueAction.Execute(driver);
	   		}catch (Exception e){
	   			throw(e);
	   		}
	   		Log.endTestCase("JIRA_Test_001");
	   		
	   	 }
	   	@Test (priority = 1,enabled = true)
	   	public  void executeSecond_TC() throws Exception{
	   		 
	   		Log.startTestCase("JIRA_Test_002 - Edit an Issue");	   		
	   		try{
	   			EditIssueAction.Execute(driver,1);	  
	   		}catch (Exception e){
	   			throw(e);
	   		}       
	   		Log.endTestCase("JIRA_Test_002");
	   		
	   	 }
	   	@Test (priority = 2)
	   	public  void executeThird_TC() throws Exception{
	   		 
	   		Log.startTestCase("JIRA_Test_003 - Search an Issue");
	   		try{
	   			EditIssueAction.Execute(driver,2);	  
	   		}catch (Exception e){
	   			throw(e);
	   		}
	   		Log.endTestCase("JIRA_Test_003");
	   		
	   	 }
	   	
	   	@BeforeSuite
	   	
	    public void beforeSuite() throws Exception {
	   		
	   		Log.info("BeforeSuite called");
	    	DOMConfigurator.configure("log4j.xml");
	   		ExcelUtils.setExcelFile(Constants.PATH_TESTDATA + Constants.FILE_TESTDATA,"Sheet1");	   		
	   	}
	   	
	   	@BeforeMethod

	    public void beforeMethod() throws Exception {
	   			        	   	 	        	        		    		 
	        driver = new FirefoxDriver();	        
	        driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
	        driver.manage().deleteAllCookies();	        	        
	        driver.get(Constants.URL);	        
	        Log.info("Web application launched");
	        
	        try{
	        	SignInAction.Execute(driver);
	        }catch (Exception e){
	        	throw(e);
	        }
	   	}
	   	@AfterMethod
	    
	    public void afterMethod() {
	   		Log.info("AfterSuite called");
	   	    driver.quit();

	   }
 
}
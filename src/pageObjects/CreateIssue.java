package pageObjects;
/*
 * This Class serves as a Object repository of all the required objects
 * on the Create issue page of the JIRA application
 */
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.support.ui.Select;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CreateIssue {
	private static WebElement element = null;
	/**
     * Attempts to click on an element multiple times (to avoid stale element
     * exceptions caused by rapid DOM refreshes)
     *
     * @param d
     *            The WebDriver
     * @param by
     *            By element locator
     */
	public static void dependableClick(WebDriver d, By by)
    {
        final int MAXIMUM_WAIT_TIME = 20;
        final int MAX_STALE_ELEMENT_RETRIES = 5;

        WebDriverWait wait = new WebDriverWait(d, MAXIMUM_WAIT_TIME);
        int retries = 0;
        while (true)
        {
            try
            {
                wait.until(ExpectedConditions.elementToBeClickable(by)).click();                
                return;
            }
            catch (StaleElementReferenceException e)
            {
                if (retries < MAX_STALE_ELEMENT_RETRIES)
                {
                    retries++;
                    continue;
                }
                else
                {
                    throw e;
                }
            }
        }
    }
	public static WebElement txtbx_ProjectField(WebDriver driver){
		dependableClick(driver,By.id("project-field"));
        element = driver.findElement(By.xpath(".//*[@id='project-field']"));        
        return element;

        }
	public static WebElement txtbx_IssueTypeField(WebDriver driver){
		 
		dependableClick(driver,By.id("issuetype-field"));
		element = driver.findElement(By.id("issuetype-field"));
        return element;

        }
	public static WebElement txtbx_SummaryField(WebDriver driver){
		
		dependableClick(driver,By.id("summary")); 
        element = driver.findElement(By.id("summary"));
        return element;

        }
	public static WebElement txtbx_DescriptionField(WebDriver driver){
		
		dependableClick(driver,By.id("description")); 
        element = driver.findElement(By.id("description"));
        return element;

        }
	
	public static WebElement btn_CreateIssueSubmit(WebDriver driver){
		
		element = driver.findElement(By.id("create-issue-submit"));
		return element;
	}
	

}

package pageObjects;
/*
 * This Class serves as a Object repository of all the required objects
 * on the Home page of the JIRA application
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import pageObjects.CreateIssue;

public class HomePage {

	private static WebElement element = null;
	
		public static WebElement lnk_MyAccount(WebDriver driver){
		
			element = driver.findElement(By.id("user-options"));						
			return element;
		
			}
	
		public static WebElement lnk_LogOut(WebDriver driver){
					
			element = driver.findElement(By.xpath(".//*[@id='log_out']"));				
			return element;
		
		}
		public static WebElement lnk_CreateIssue(WebDriver driver){
			
			CreateIssue.dependableClick(driver,By.xpath(".//*[@id='create_link']"));
			element = driver.findElement(By.xpath(".//*[@id='create_link']"));				
			return element;
			
		}
		public static WebElement lnk_SearchIssue(WebDriver driver){
 
			element = driver.findElement(By.xpath(".//*[@id='find_link']"));	       
			return element;
			
		}
		
		public static WebElement lnk_SearchIssueReportedbyme(WebDriver driver){

			element = driver.findElement(By.xpath(".//*[@id='filter_lnk_reported_lnk']"));
			return element;
			
		}
		
		public static WebElement txtbx_QuickSearch(WebDriver driver){

			element = driver.findElement(By.xpath(".//*[@id='quickSearchInput']"));
			return element;
			
		}
		
		public static WebElement btn_EditIssue(WebDriver driver){
			element = driver.findElement(By.xpath(".//*[@id='edit-issue']"));
			return element;
			
		}
		
		
		
		
		
		
}

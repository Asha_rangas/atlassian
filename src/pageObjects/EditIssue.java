package pageObjects;
/*
 * This Class serves as a Object repository of all the required objects
 * on the Edit issue page of the JIRA application
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class EditIssue {
	private static WebElement element = null;
	public static WebElement txtbx_SummaryField(WebDriver driver){
		
	//	dependableClick(driver,By.id("summary")); 
        element = driver.findElement(By.xpath(".//*[@id='summary']"));
        return element;

        }
	public static WebElement txtbx_DescriptionField(WebDriver driver){
		
	//	dependableClick(driver,By.id("description")); 
        element = driver.findElement(By.xpath(".//*[@id='description']"));
        return element;

        }
	public static WebElement btn_EditIssueSubmit(WebDriver driver){
		
		element = driver.findElement(By.xpath(".//*[@id='edit-issue-submit']"));
		return element;
	}

}

package pageObjects;
/*
 * This Class serves as a Object repository of all the required objects
 * on the Search page of the JIRA application
 */
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import utility.Constants;

public class SearchPage {
	private static List<WebElement> all;
			
	private static List<String> get_ListofIssues(WebDriver driver){
		WebDriverWait wait = new WebDriverWait(driver, 30);
		wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(By.xpath("//div[@class='list-content']"))); 
		driver.findElements(By.xpath("//div[@class='list-content']")); 
	    all = driver.findElements(By.xpath("//ol[@class='issue-list']/li")); 
	     
		System.out.println("length of allelements="+all.size());
	       List<String> myList = new ArrayList<>();
	       for (int i = 1; i < all.size()+1; i++) {
	           WebElement linkElement = driver
	                   .findElement(By
	                           .xpath("//div[@class='list-content']/ol/li[" + i
	                                   + "]/a/span[1]"));
	
	           System.out.println(linkElement.getText());
	           myList.add(linkElement.getText());
	       }
	       return myList;
	}
	public static String get_DefectId(WebDriver driver,int flag){
		List<String> issue = get_ListofIssues(driver);
		
		int size = issue.size();
		System.out.println("length of the myList="+size);
		String searchDefect,editDefect;
		if (size > 1) { 
			searchDefect = issue.get(0) ;
			editDefect = issue.get(1);
		} else if (size == 1) {
			searchDefect = issue.get(0) ;
		    editDefect = issue.get(0);
		} else {
			searchDefect = Constants.ERROR ;
			editDefect = Constants.ERROR ;
		}
	    if(flag == 1){
	    	   return editDefect;
	    }else if(flag == 2){
	    	   return searchDefect;
	    }
		return editDefect;
	}


}

package pageObjects;
/*
 * This Class serves as a Object repository of all the required objects
 * on the Login  page of the JIRA application
 */
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import utility.Log;

public class LogInPage {
	 private static WebElement element = null;
	 
	    public static WebElement txtbx_UserName(WebDriver driver){
	 
	         element = driver.findElement(By.id("username"));
	         
	         Log.info("Username text box found");
	 
	         return element;
	 
	         }
	 
	   public static WebElement txtbx_Password(WebDriver driver){
	 
	         element = driver.findElement(By.id("password"));
	         
	         Log.info("Password text box found");
	 
	         return element;
	 
	         }
	 
	   public static WebElement btn_LogIn(WebDriver driver){
	 
	         element = driver.findElement(By.id("login-submit"));
	         
	         Log.info("Submit button found");
	 
	         return element;
	 
	         }
}
